# Copyright (C) 2019 Emcraft Systems
# Build the NXP UUU tool and install it to the images directory,
# along with the uuu scripts for IMX8MN LEGO.

SRC_URI = "git://github.com/codeauroraforum/mfgtools.git;protocol=https;branch=master"

SRC_URI += " \
   file://uuu.script-debug \
"

LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://LICENSE;md5=38ec0c18112e9a92cffc4951661e85a5"

S = "${WORKDIR}/git"
SRCREV = "uuu_1.3.191"

DEPENDS = "libusb1-native libzip-native"

inherit cmake native deploy

# We need this task to automate building of this recipe via
# IMAGE_BOOTFILES_DEPENDS += "uuu-native:do_deploy"
addtask deploy after do_install

do_deploy () {
	   install -m 0755 ${B}/uuu/uuu  ${DEPLOYDIR}
	   sed -e "s/debug-_machine/_machine/g" ${WORKDIR}/uuu.script-debug > ${WORKDIR}/uuu.script-${MACHINE}
	   sed -i -e "s/_machine/${MACHINE}/g" ${WORKDIR}/uuu.script-${MACHINE}
	   install -m 0644 ${WORKDIR}/uuu.script-${MACHINE}  ${DEPLOYDIR}

	   sed -e "s/_machine/${MACHINE}/g" ${WORKDIR}/uuu.script-debug > ${WORKDIR}/uuu.script-debug-${MACHINE}
	   install -m 0644 ${WORKDIR}/uuu.script-debug-${MACHINE}  ${DEPLOYDIR}

}
