FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

IMX_KERNEL_CONFIG_AARCH64 = "imx_v8_defconfig"

SRC_URI = "git://git@gitlab.com/emcraft/imx8mm/cube/linux-imx8.git;protocol=ssh;branch=imx8m-ga-5.4.70-2.3.0"

SRCREV = "${AUTOREV}"
LOCALVERSION = "${CONFIG_LINUX_LOCALVERSION}"
