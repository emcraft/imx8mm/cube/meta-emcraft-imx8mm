# Copyright (C) 2019 Emcraft Systems

# This appends to meta-fsl-bsp-release/imx/meta-bsp/recipes-bsp/u-boot/u-boot-imx_2017.03.bb

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "git://git@gitlab.com/emcraft/imx8mm/cube/u-boot-imx8.git;protocol=ssh;branch=imx8m-ga-5.4.70-2.3.0"

SRCREV = "${AUTOREV}"
LOCALVERSION = "${CONFIG_UBOOT_LOCALVERSION}"

do_configure_append () {
    if [ ! -z "${UBOOT_ENV_FDT_FILE}" ]; then
        for i in ${UBOOT_MACHINE}; do
            sed -ie '/CONFIG_DEFAULT_FDT_FILE/d' ${S}/configs/`echo $i | sed 's/_config/_defconfig/'`
            echo 'CONFIG_DEFAULT_FDT_FILE="${UBOOT_ENV_FDT_FILE}"' >> ${S}/configs/`echo $i | sed 's/_config/_defconfig/'`
        done
    fi
}
