# Copyrigh (C) 2020 Emcraft Systems
# Root filesystem image

inherit core-image

IMAGE_FEATURES_append = "	\
    hwcodecs	      		\
    package-management		\
    ssh-server-openssh		\
    empty-root-password		\
"
IMAGE_INSTALL_append = "	\
    coreutils	       		\
    bash			\
    dhcp-client			\
    inetutils			\
    kernel-modules		\
    util-linux			\
"


# Customize the rootfs by running post-installation scripts
ROOTFS_POSTPROCESS_COMMAND += "custom_files; "
IMX_DISTRO_NAME ?= "i.MX 8M Plus Distro Release"
IMX_DISTRO_VERSION ?= ""

# Add optional "debug" filesystem files.
inc = "${@bb.utils.contains('BUILDTYPE', 'debug', 'image-emcraft-debug.inc', '', d)}"
require ${inc}

custom_files() {
		  install -d ${IMAGE_ROOTFS}${sysconfdir}/
		  if [ "${BUILDTYPE}" = "debug" ]; then
		     bbwarn "Using debug configuration."
		  else
		     bbwarn "Using production configuration."
		  fi
		  if [ -n "${IMX_DISTRO_NAME}" ]; then
		     printf "${IMX_DISTRO_NAME} " > ${IMAGE_ROOTFS}${sysconfdir}/issue
		     printf "${IMX_DISTRO_NAME} " > ${IMAGE_ROOTFS}${sysconfdir}/issue.net
		     if [ -n "${IMX_DISTRO_VERSION}" ]; then
			printf "${IMX_DISTRO_VERSION} " >> ${IMAGE_ROOTFS}${sysconfdir}/issue
			printf "${IMX_DISTRO_VERSION} " >> ${IMAGE_ROOTFS}${sysconfdir}/issue.net
		     fi
		     printf "\\\n \\\l\n" >> ${IMAGE_ROOTFS}${sysconfdir}/issue
		     echo >> ${IMAGE_ROOTFS}${sysconfdir}/issue
		     echo "%h"    >> ${IMAGE_ROOTFS}${sysconfdir}/issue.net
		     echo >> ${IMAGE_ROOTFS}${sysconfdir}/issue.net
		  fi
}
