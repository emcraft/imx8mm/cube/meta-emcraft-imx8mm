# Copyrigh (C) 2020 Emcraft Systems
# Root filesystem image for debugging

require dynamic-layers/qt5-layer/recipes-fsl/images/fsl-image-qt5-validation-imx.bb

IMAGE_INSTALL_remove = "packagegroup-imx-ml"

# Generate <rootfs-name>-debug.* images
IMAGE_BASENAME = "${PN}-debug"

IMAGE_FEATURES_append = "		\
    nfs-client				\
    ssh-server-openssh			\
    tools-testapps			\
    debug-tweaks			\
    tools-debug				\
    tools-profile			\
    eclipse-debug			\
    empty-root-password		       \
    "

IMAGE_INSTALL_append = "		\
    usbutils				\
    i2c-tools				\
    opkg				\
    socat				\
    links				\
    openssh-sftp-server			\
    vim					\
    libpng				\
    zlib				\
    htop				\
    iotop				\
    procps				\
    perf				\
    libpng				\
    libasound				\
    libcrypto				\
    libcurl				\
    iperf3				\
    netcat				\
    spitools				\
    alsa-utils				\
    bridge-utils			\
    dhcp-client				\
    rsync				\
    iw					\
    libnl				\
    libnl-genl				\
    libusb1				\
    libgcc				\
    gdb					\
    lsof				\
    psmisc				\
    cpufrequtils			\
    iputils				\
    evtest				\
    lcdtest				\
"

# These are causing conflicts in SDK, not installed in the image anyway.
PACKAGE_EXCLUDE = "libgl-imx-dev"

SDKIMAGE_FEATURES_append = " dev-pkgs dbg-pkgs staticdev-pkgs"

IMAGE_ROOTFS_EXTRA_SPACE = "548576"

# Customize the rootfs by running post-installation scripts
ROOTFS_POSTPROCESS_COMMAND += "custom_files_debug; "

custom_files_debug() {
		     bbwarn "Including debug filesystem files."
#		     install -d ${IMAGE_ROOTFS}${base_bindir}/
#		     install -m 0755 ${THISDIR}/files/usb_vbus_enable.sh ${IMAGE_ROOTFS}${base_bindir}/
}
